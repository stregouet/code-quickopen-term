'use strict';
import * as vscode from 'vscode';

async function main(terminals: readonly vscode.Terminal[]) {
	if (terminals.length === 0) {
		vscode.window.showInformationMessage('no terminal currently opened');
		return;
	}
	const pids = await Promise.all(terminals.map(t => t.processId));
	const choices: vscode.QuickPickItem[] = terminals.map((t, idx) => ({label: t.name, description: `with pid "${pids[idx]}"` }));
	const userChoice = await vscode.window.showQuickPick<vscode.QuickPickItem>(choices, {
		placeHolder: `select term you want to focus`
	});
	if (userChoice === undefined || userChoice === null) {
		vscode.window.showInformationMessage('no terminal selected');
		return;
	}
	const userChoicePid = userChoice.description!.match(/with pid "(\d+)"/)![1];
	const selectedPid = pids.filter(p => p === Number(userChoicePid));
	const selectedIdx = pids.indexOf(selectedPid[0]);
	terminals[selectedIdx].show();
}


export function activate(context: vscode.ExtensionContext) {
    let disposable = vscode.commands.registerCommand('extension.quickOpenTerm', () => {
        main(vscode.window.terminals).catch(err => {
			console.error('something wrong', err);
			vscode.window.showErrorMessage('something goes wrong with `code-quickopen-term` extension, see devtools console');
		});
    });

    context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {
}
